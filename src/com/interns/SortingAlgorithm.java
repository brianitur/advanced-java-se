package com.interns;

import java.util.Arrays;

public class SortingAlgorithm {
    public int[] bubbleSort(int[] a)
    {
        for (int k = 0; k < a.length; k ++)
        {
            for (int i = k + 1; i < a.length; i ++)
            {
                if (a[k] > a[i])
                {
                    int temp = a[i]; // save a[i] before overwriting it
                    a[i] = a[k];
                    a[k] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(a));
        return a;
    }
    public int[] bubbleSortWithRecursion(int[] a, int k)
    {
        if (k < a.length)
        {
            for (int i = 0; i < a.length - 1; i ++)
            {
                if (a[i] > a[i + 1])
                {
                    int temp = a[i];
                    a[i] = a[i + 1];
                    a[i + 1] = temp;
                }
            }
            k ++;
            return bubbleSortWithRecursion(a, k);
        }
        System.out.println(Arrays.toString(a));
        return a;
    }
}
