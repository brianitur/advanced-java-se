package com.interns.catII;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArraySearch {
    public int[] solve(int[] copy, int[] original)
    {
        List<Integer> missingNumbers = new ArrayList<>();
        for (int k = 0; k < original.length; k ++)
        {
            if (this.isMissingInCopy(original[k], copy) || this.hasMismatchingFrequencyChanged(original[k], copy, original))
                missingNumbers.add(original[k]);
        }
        int[] missing = new int[missingNumbers.size()];
        for (int k = 0; k < missing.length; k ++)
            missing[k] = missingNumbers.get(k);
        Arrays.sort(missing);
        return missing;
    }
    private boolean hasMismatchingFrequencyChanged(int s, int[] copy, int[] original)
    {
        int copyFrequency = 0, originalFrequency = 0;
        for (int k = 0; k < copy.length; k ++)
            if (copy[k] == s)
                copyFrequency ++;
        for (int k = 0; k < original.length; k ++)
            if (original[k] == s)
                originalFrequency ++;
        return copyFrequency == originalFrequency;
    }
    private boolean isMissingInCopy(int n, int[] a)
    {
        for (int k = 0; k < a.length; k ++)
        {
            if (a[k] == n)
            {
                return false;
            }
        }
        return true;
    }
}
