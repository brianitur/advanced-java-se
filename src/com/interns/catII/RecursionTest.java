package com.interns.catII;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

public class RecursionTest {
    @Test
    public void testStrCount()
    {
        Assert.assertEquals(2, new Recursion().strCount("catcowcat", "cat"));
        Assert.assertEquals(1, new Recursion().strCount("catcowcat", "cow"));
        Assert.assertEquals(0, new Recursion().strCount("catcowcat", "dog"));
    }
    @Test
    public void testPairStar()
    {
        /**
         * pairStar("hello") → "hel*lo"
         * pairStar("xxyy") → "x*xy*y"
         * pairStar("aaaa") → "a*a*a*a"
         */
        Assert.assertEquals("hel*lo", new Recursion().pairStar("hello"));
        Assert.assertEquals("x*xy*y", new Recursion().pairStar("xxyy"));
        Assert.assertEquals("a*a*a*a", new Recursion().pairStar("aaaa"));
    }
}
