package com.interns.catII;

public class SuperDigit {
    public long solve(String n, int k)
    {
        String temp = n;
        for (int i = 0; i < k - 1; i ++)
        {
            System.out.println("N = " + n);
            n += temp; // concatenate the string k times
        }
        long superDigit;
        do {
            // calculate the superdigit
            superDigit = simple_solve(n);
            n = String.valueOf(superDigit);
        } while (superDigit > 10);
        return superDigit;
    }
    public long simple_solve(String n)
    {
        long x = Long.parseLong(n);
        if (x / 10 > 0)
        {
            long superDigit = x % 10;
            return superDigit + simple_solve(String.valueOf(Long.valueOf((x / 10) + "").longValue()));
        }
        return x;
    }
}
