package com.interns.catII;

public class Recursion {
    /**
     *
     * @param text catdefcat || cccccc
     * @param key cat || cc
     * @return
     */
    public int strCount(String text, String key)
    {
        // Base Condition (Exit Condition)
        if (!text.contains(key) || text.isEmpty())
            return 0;
        // Actual Work to be done
        if (text.substring(0, key.length()).equals(key))
        {
            return 1 + strCount(text.substring(key.length()), key);
        }
        else
            return strCount(text.substring(1), key);
    }

    public String pairStar(String text)
    {
        if (text.length() <= 1)
            return text;
        // text length = 1
        if (text.charAt(0) == text.charAt(1)) //hello l == l
            return Character.toString(text.charAt(0)) + "*" + pairStar(text.substring(1)); // hel* + pairstar(lo)
        else
            return Character.toString(text.charAt(0)) + pairStar(text.substring(1)); // h + pairstar(ello)
    }

}
