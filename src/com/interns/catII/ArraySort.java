package com.interns.catII;

public class ArraySort {
    public String[] solve(String[] s)
    {
        for (int k = 0; k < s.length; k ++)
        {
            for (int i = k + 1; i < s.length; i ++)
            {
                if (Integer.parseInt(s[i]) < Integer.parseInt(s[k]))
                {
                    String temp = s[k];
                    s[k] = s[i];
                    s[i] = temp;
                }
            }
        }
        return s;
    }
}
