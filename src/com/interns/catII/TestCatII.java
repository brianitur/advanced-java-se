package com.interns.catII;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class TestCatII {
    @Test
    public void testSuperDigit()
    {
        Assert.assertEquals(8, new SuperDigit().solve("9875", 4));
        Assert.assertEquals(3, new SuperDigit().solve("148", 3));
    }

    @Test
    public void testArraySort()
    {
        Assert.assertEquals("[1, 3, 150, 200]", Arrays.toString(new ArraySort().solve(new String[]{"1", "200", "150", "3"})));
    }

    @Test
    public void testArraySearch()
    {
        Assert.assertEquals("[4, 6]", Arrays.toString(new ArraySearch().solve(new int[]{7, 2, 5, 3, 5, 3, 1000}, new int[]{7, 1000, 2, 5, 4, 6, 3, 5, 3})));
    }
}
