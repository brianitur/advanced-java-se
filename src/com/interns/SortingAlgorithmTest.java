package com.interns;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class SortingAlgorithmTest {

    @Test
    void bubbleSort() {
        int[] a = new int[]{3, 4, 5, 2, 2, 1, 1, 3, 4, 5, 6, 65, 4, 3};
        Assert.assertEquals("[1, 1, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 6, 65]", Arrays.toString(new SortingAlgorithm().bubbleSort(a)));
    }
    @Test
    void bubbleSortWithRecursion() {
        int[] a = new int[]{3, 4, 5, 2, 2, 1, 1, 3, 4, 5, 6, 65, 4, 3};
        Assert.assertEquals("[1, 1, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 6, 65]", Arrays.toString(new SortingAlgorithm().bubbleSortWithRecursion(a, 0)));
    }
}