package com.interns;


import java.util.ArrayList;
import java.util.List;

/**
 *
 * n = 2
 * line 1 ->
 * line 2 -> 1 2 3 4 5 6
 * 2 3 => (x = 2, y = 3)
 * y = index position
 * x = line 1 or line 2
 *
 */
public class SolutionFour {

    public void time()
    {
        for (int i = 0; i < 1000000; i ++)
            System.out.println(i); // take more than 1 second
    }

    public List<String> solve(List<List<Integer>> lines, List<List<Integer>> queries)
    {
        List<String> returns = new ArrayList<>();
        for (List<Integer> query : queries)
        {
            int x = query.get(0) - 1; // position of line we want
            int y = query.get(1) - 1; // position of element we want
            try {
                List<Integer> numbers = lines.get(x); // line we want (exception)
                int number = numbers.get(y); // element we want in the line (exception)
                returns.add(number + "");
            } catch (IndexOutOfBoundsException e) {
                returns.add("ERROR!");
            }
        }
        return returns;
    }
}
