package com.interns;

import java.text.DecimalFormat;

public class PlusMinus {
    public String solve(int[] a)
    {
        int positive = 0, negative = 0, zero = 0;
        for (int k : a)
        {
            if (k == 0)
                zero ++;
            else if (k > 0)
                positive ++;
            else
                negative ++;
        }
        double positiveRatio = (double) positive / a.length;
        double negativeRatio = (double) negative / a.length;
        double zeroRatio = (double) zero / a.length;
        return String.format("%.6f", positiveRatio) + "\n" + String.format("%.6f", negativeRatio) + "\n" + String.format("%.6f", zeroRatio);
    }
}
