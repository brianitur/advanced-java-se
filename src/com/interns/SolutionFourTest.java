package com.interns;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

class SolutionFourTest {
    SolutionFour solutionFour;
    @org.junit.Test(timeout = 1)
    public void testTime()
    {
        solutionFour.time();
    }

    @Test
    void solve() {
        List<List<Integer>> lines = new ArrayList<>();
        List<List<Integer>> queries = new ArrayList<>();
        List<Integer> line1 = new ArrayList<>();
        line1.add(41);
        line1.add(77);
        line1.add(74);
        line1.add(22);
        line1.add(44);
        lines.add(line1);
        List<Integer> line2 = new ArrayList<>();
        line2.add(12);
        lines.add(line2);
        List<Integer> line3 = new ArrayList<>();
        line3.add(37);
        line3.add(34);
        line3.add(36);
        line3.add(52);
        lines.add(line3);
        List<Integer> line4 = new ArrayList<>();
        lines.add(line4);
        List<Integer> line5 = new ArrayList<>();
        line5.add(20);
        line5.add(22);
        line5.add(33);
        lines.add(line5);
        List<Integer> query1 = new ArrayList<>();
        query1.add(1);
        query1.add(3);
        queries.add(query1);
        List<Integer> query2 = new ArrayList<>();
        query2.add(3);
        query2.add(4);
        queries.add(query2);
        List<Integer> query3 = new ArrayList<>();
        query3.add(3);
        query3.add(1);
        queries.add(query3);
        List<Integer> query4 = new ArrayList<>();
        query4.add(4);
        query4.add(3);
        queries.add(query4);
        List<Integer> query5 = new ArrayList<>();
        query5.add(5);
        query5.add(5);
        queries.add(query5);
        Assert.assertEquals("[74, 52, 37, ERROR!, ERROR!]", solutionFour.solve(lines, queries).toString());
    }
}