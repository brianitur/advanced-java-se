package com.interns.MyThread;

import java.util.Random;

public class MaxValue {
    public static int maxValue;
    public static int a[];
    public MaxValue()
    {
        this.createArray();
    }
    // step 1. Create the array
    public void createArray()
    {
        a = new int[10000000];
        for (int k = 0; k < a.length; k ++)
        {
            int value = new Random().nextInt();
            a[k] = value;
        }
    }
    public void serialProcessing(){
        long startTime = System.nanoTime();
        int maxValue = Integer.MIN_VALUE;
        for (int k = 0; k < a.length; k ++)
        {
            if (a[k] > maxValue)
                maxValue = a[k];
        }
        long endTime = System.nanoTime();
        System.out.println("Single Processing: Max Value: " + maxValue + ", Time: " + (endTime - startTime));
    }
    public void parallelProcessing() throws InterruptedException {
        this.execute();
    }
    public void execute() throws InterruptedException {
        long startTime = System.nanoTime();
        maxValue = 0;
        MaxValueThread[] threads = new MaxValueThread[4];
        int chunkSize = a.length / 4; // 2500
        long sTime = System.nanoTime();
        for (int k = 0; k < threads.length; k ++)
        {
            int startIndex = chunkSize * k;
            int endIndex = startIndex + chunkSize;
            threads[k] = new MaxValueThread(startIndex, endIndex);
            threads[k].start();
        }
        long eTime = System.nanoTime();
        for (int k = 0; k < threads.length; k ++)
            threads[k].join();
        long endTime = System.nanoTime();
        System.out.println("Execute: Max Value: " + maxValue + ", Time: " + (endTime - startTime)  + " (Time wasted: " + (eTime - sTime) + ")");
    }
    public void executeSplit() throws InterruptedException {
        long startTime = System.nanoTime();
        maxValue = 0;
        MaxValueThreadSplit[] threads = new MaxValueThreadSplit[4];
        int chunkSize = a.length / 4; // 2500
        // Creating 4 arrays (a.length /4 elements)
        long sTime = System.nanoTime();
        for (int k = 0; k < threads.length; k ++)
        {
            int[] b = new int[chunkSize]; //new array
            for (int j = (k * chunkSize); j < (k * chunkSize) + chunkSize; j ++)
            {
                int innerArrayIndex = j - (k * chunkSize);
                b[innerArrayIndex] = a[j];
            }
            threads[k] = new MaxValueThreadSplit(b);
            threads[k].start();
        }
        long eTime = System.nanoTime();
        for (int k = 0; k < threads.length; k ++)
            threads[k].join();
        long endTime = System.nanoTime();
        System.out.println("Execute Split: Max Value: " + maxValue + ", Time: " + (endTime - startTime) + " (Time wasted: " + (eTime - sTime) + ")");
    }
}
// Array has 1, 3, 4, 5, 6, 7, 8, 6, 4, 3, 2, 2
// Thread 1 => startIndex = 0, endIndex = 3 (1, 3, 4)
// 4 copies of this class
class MaxValueThreadSplit extends Thread {
    private int[] a;
    public MaxValueThreadSplit(int[] a)
    {
        this.a = a;
    }
    public void run()
    {
        int maxValue = Integer.MIN_VALUE;
        for (int k = 0; k < a.length; k ++)
        {
            if (a[k] > maxValue)
                maxValue = a[k];
        }
        if (MaxValue.maxValue < maxValue)
            MaxValue.maxValue = maxValue;
    }
}
class MaxValueThread extends Thread {
    private int startIndex;
    private final int endIndex;
    public MaxValueThread(int startIndex, int endIndex)
    {
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }
    public void run(int n)
    {
        // do nothing
    }
    public void run()
    {
        int threadMaxValue = MaxValue.a[startIndex];
        for (int k = startIndex + 1; k < endIndex; k ++)
        {
            if (threadMaxValue < MaxValue.a[k])
                threadMaxValue = MaxValue.a[k];
        }
        if (MaxValue.maxValue < threadMaxValue)
            MaxValue.maxValue = threadMaxValue;
    }
}
