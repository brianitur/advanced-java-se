package com.interns.MyThread;

public class ReverseHello extends Thread {
    static int counter = 0;
    @Override
    public void run() {
        counter ++;
        String message = "Hello from Thread " + counter + "!";
        if (counter < 50)
        {
            ReverseHello reverseHello = new ReverseHello();
            reverseHello.start();
            try {
                reverseHello.join(); // thread 1 will wait for thread 2 which will wait for thread 3 which will wait for thread 4
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(message);
        }
    }
}
