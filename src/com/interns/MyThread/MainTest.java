package com.interns.MyThread;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    @Test
    void testFactorialWithRecursion()
    {
        Assert.assertEquals(120, Main.factorialWithRecursion(5));
    }
    @Test
    void testFactorialWithForLoop()
    {
        Assert.assertEquals(120, Main.factorialWithForLoop(5));
    }

}