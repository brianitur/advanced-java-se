package com.interns.MyThread;

/**
 * Write a program called ReverseHello.java that creates a thread (let's call it Thread 1).
 * Thread 1 creates another thread (Thread 2); Thread 2 creates Thread 3; and so on, up to Thread 50.
 * Each thread should print "Hello from Thread <num>!", but you should structure your program such that the threads print their greetings in reverse order.
 * T1
 *  |_ T2
 *      |_ T3
 *          |_ T4
 * T1
 * T2
 * T3
 * T4
 */


/**
 * O(
 */
public class Main {
    public static void main(String[] args) throws InterruptedException {
        int a = 2 + 5; // 2 will be pushed to stack, followed by 5, the + will go to the ALU -> 7
        long sTime = System.nanoTime();
        int factorial = factorialWithRecursion(1000);
        long eTime = System.nanoTime();
        long recursionTime = eTime - sTime;
        sTime = System.nanoTime();
        factorial = factorialWithForLoop(1000);
        eTime = System.nanoTime();
        long loopTime = eTime - sTime;
        System.out.println("Recursion: " + recursionTime + ", Loop: " + loopTime);
    }

    public static int factorialWithForLoop(int number)
    {
        int factorial = 1;
        for (int k = number; k > 1; k --)
        {
            factorial = factorial * k;
        }
        return factorial;
    }

    public static int factorialWithRecursion(int number)
    {
        if (number == 0 || number == 1)
            return 1;
        return number * factorialWithRecursion(number - 1);
    }

}

