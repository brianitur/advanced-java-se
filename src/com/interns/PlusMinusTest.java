package com.interns;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlusMinusTest {

    @Test
    void solve() {
        int[] a = new int[]{1, 1, 0, -1, -1};
        String expectedString = "0.400000\n0.400000\n0.200000";
        assertEquals(expectedString, new PlusMinus().solve(a));
    }
}