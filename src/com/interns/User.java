package com.interns;

import java.util.Comparator;

public class User implements Comparable<User> {
    private String name, email;

    public String getName() {
        return name;
    }

    public User(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String toString()
    {
        return "{ Name: " + this.name + ", Email: " + this.email + "}";
    }
    @Override
    public int compareTo(User o) {
        // comparing 2 things (this and o)
        // return -1 if this < o
        // return 0 if this == o
        // return 1 if this > 0
        // apply order of precedence (Name is first)
        // "Brian" "bryanitur@gmail.com"
        // "Brian" "iturbryan@gmail.com"
        if (this.name.compareTo(o.name) == 0)
        {
            return this.email.compareTo(o.email);
        }
        else
            return this.name.compareTo(o.name);
    }
}
